import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

val sharedSettings = Seq(
  organization := "tf.bug",
  name := "puyix",
  version := "0.1.0",
  scalaVersion := "2.12.6",
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification"
  ),
  libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-core" % "1.2.0",
    "org.typelevel" %%% "cats-effect" % "1.0.0-RC2",
    "io.monix" %%% "monix" % "3.0.0-RC1",
    "org.typelevel" %%% "spire" % "0.16.0",
    "org.scalactic" %%% "scalactic" % "3.0.5",
    "org.scalatest" %%% "scalatest" % "3.0.5" % "test"
  )
)

lazy val puyix =
  crossProject(JSPlatform, JVMPlatform)
  .crossType(CrossType.Full)
  .in(file("."))
  .settings(sharedSettings)
  .jvmSettings(/* ... */)
  .jsSettings(/* ... */)

lazy val puyixJS     = puyix.js
lazy val puyixJVM    = puyix.jvm

resolvers += "Typesafe" at "https://repo.typesafe.com/typesafe/releases/"
