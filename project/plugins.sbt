resolvers ++= Seq(
  "Typesafe Repository @ Plugins" at "https://repo.typesafe.com/typesafe/releases/",
  "Artima Maven Repository @ Plugins" at "http://repo.artima.com/releases/"
)
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")
addSbtPlugin("com.codacy" % "sbt-codacy-coverage" % "1.3.14")
addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "0.6.0")
addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.24")
